package com.ecommerce.microcommerce.web.controller;


import com.ecommerce.microcommerce.dao.ProductDao;
import com.ecommerce.microcommerce.model.Category;
import com.ecommerce.microcommerce.model.Product;
import com.ecommerce.microcommerce.web.exceptions.ProduitIntrouvableException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Api("API's CRUD for Products")
public class ProductController {

    @Autowired
    private ProductDao productDao;

    //--------------------------- Get methods

    @ApiOperation(value = "This methode get all products.")
    @GetMapping(value = "/Products")
    public ResponseEntity<List<Product>> lstProducts() {

        List<Product> lstProduct = productDao.findAll();

        if (lstProduct == null)
            return ResponseEntity.noContent().build();

        
        return ResponseEntity.ok(lstProduct);
    }

    @ApiOperation(value = "This methode get all categorys.")
    @GetMapping(value = "/Products/Category/")
    public ResponseEntity<List<Category>> lstCategorys() {
        return ResponseEntity.ok(new ArrayList<>(Arrays.asList(Category.values())));
    }

    @ApiOperation(value = "This methode return a products list by category.")
    @GetMapping(value = "/Products/Category/{category}")
    public List<Product> findProductByCategory(@PathVariable String category) {
        return productDao.findByCategory(Category.fromName(category));
    }

    @ApiOperation(value = "This methode find a product by reference.")
    @GetMapping(value = "/Products/{reference}")
    public Product findByReference(@PathVariable String reference) {
        Product product = productDao.findByReference(reference);

        if (product == null)
            throw new ProduitIntrouvableException("Le produit avec la reference " + reference + " est INTROUVABLE.");

        return product;
    }

    @ApiOperation(value = "This methode find a product list by a search.")
    @GetMapping(value = "/Products/Search/{search}")
    public List<Product> findBySearch(@PathVariable String search) {
        List<Product> lstProduct = productDao.findByNameContainingOrDescriptionContainingOrReferenceContaining(search, search,search);
        return lstProduct;
    }


    //--------------------------- Post methods

    @ApiOperation(value = "This methode create a product in database.")
    @PostMapping(value = "/Products")
    public ResponseEntity<Void> addProduct(@Valid @RequestBody Product product) {

        Product productAdded = productDao.save(product);

        if (productAdded == null)
            return ResponseEntity.noContent().build();

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(productAdded.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    //--------------------------- Put methods

    @ApiOperation(value = "This methode update a product in database.")
    @PutMapping(value = "/Products")
    public void updateProduit(@RequestBody Product product) {
        productDao.save(product);
    }

    //--------------------------- Delete methods

    @ApiOperation(value = "This methode delete a product in database and use the id product.")
    @DeleteMapping(value = "/Products/{id}")
    public void deleteProduct(@PathVariable long id) {
        productDao.deleteById(id);
    }







}

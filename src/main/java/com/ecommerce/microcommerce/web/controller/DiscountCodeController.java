package com.ecommerce.microcommerce.web.controller;

import com.ecommerce.microcommerce.dao.DiscountCodeDao;
import com.ecommerce.microcommerce.model.DiscountCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

/**
 * DiscountCodeController
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Api("API's discount code operations")
public class DiscountCodeController {

    @Autowired
    private DiscountCodeDao discountCodeDao;

    //--------------------------- Post methods

    @ApiOperation(value = "This methode create a Discount Code in database.")
    @PostMapping(value = "/DiscountCode")
    public ResponseEntity<Void> addDiscountCode(@Valid @RequestBody DiscountCode discountCode) {

        DiscountCode discountCodeAdded = discountCodeDao.save(discountCode);

        if (discountCodeAdded == null)
            return ResponseEntity.noContent().build();

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(discountCodeAdded.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "This methode find a discount code by code.")
    @GetMapping(value = "/DiscountCode/{code}")
    public DiscountCode findByCode(@PathVariable String code) {
        DiscountCode discountCode = discountCodeDao.findByCode(code);

        // if (discountCode == null)
        //     throw new RuntimeException("Discountcode white code" + code + " not found.");

        return discountCode;
    }
}
package com.ecommerce.microcommerce.web.controller;

import javax.validation.Valid;

import com.ecommerce.microcommerce.dao.UserDao;
import com.ecommerce.microcommerce.model.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

/**
 * UserController
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Api("API's user operations")
public class UserController {

    @Autowired
    private UserDao userDao;

    @ApiOperation(value = "This methode find a user by password, mail or username.")
    @PostMapping(value = "/User/Auth")
    public User authUser(@RequestBody User userArg) {

        User userAuth = null;
        if (userArg != null)
            userAuth = userDao.findByPasswordAndMailOrUsername(userArg.getPassword(), userArg.getMail(),
                    userArg.getUsername());

        return userAuth;

    }

    @ApiOperation(value = "This methode create a user in database.")
    @PostMapping(value = "/User")
    public ResponseEntity<Void> addUser(@Valid @RequestBody User user) {

        User userAdded = userDao.save(user);

        if (userAdded == null)
            return ResponseEntity.noContent().build();

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(userAdded.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }

}
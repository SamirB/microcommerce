package com.ecommerce.microcommerce.web.controller;


import com.ecommerce.microcommerce.model.Cart;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Api("API's cart operations")
public class CartController {


    //--------------------------- Put methods

    @ApiOperation(value = "This methode update a cart.")
    @PutMapping(value = "/Cart")
    public Cart calulateCart(@RequestBody Cart cart) {

        if (cart == null) {
            return null;
        } else {
            cart.priceCalculate();
        }

        return cart;
    }
}

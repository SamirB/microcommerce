package com.ecommerce.microcommerce.dao;

import com.ecommerce.microcommerce.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * UserDao
 */
public interface UserDao extends JpaRepository<User, Long>{

    User findByMailAndPassword(String reference, String password);

    @Query("SELECT u FROM User u WHERE u.password = :password AND (u.mail = :mail OR u.username = :username)" )
    User findByPasswordAndMailOrUsername(String password , String mail, String username);
}
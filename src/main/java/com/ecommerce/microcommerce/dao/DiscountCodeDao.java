package com.ecommerce.microcommerce.dao;

import com.ecommerce.microcommerce.model.DiscountCode;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * DiscountCodeDao
 * 
 */
public interface DiscountCodeDao extends JpaRepository<DiscountCode, Long> {

    DiscountCode findByCode(String code);
    
}
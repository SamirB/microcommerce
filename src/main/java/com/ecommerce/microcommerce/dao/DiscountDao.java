package com.ecommerce.microcommerce.dao;

import com.ecommerce.microcommerce.model.Discount;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DiscountDao extends JpaRepository<Discount, Long> {

    
}
package com.ecommerce.microcommerce.dao;

import com.ecommerce.microcommerce.model.Category;
import com.ecommerce.microcommerce.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface ProductDao extends JpaRepository<Product, Long> {

    Product findById(long id);

    Product findByReference(String reference);

    List<Product> findByNameLike(String recherche);

//    List<Product> findByCategory(String category);

    @Query("SELECT p FROM Product p WHERE p.category = :category " )
    List<Product>  findByCategory(@Param("category") Category category);

    List<Product>  findByNameContainingOrDescriptionContainingOrReferenceContaining(String search, String search2, String search3);


//    @Query("SELECT id, nom, prix FROM Product p WHERE p.prix > :prixLimit")
//    List<Product>  chercherUnProduitCher(@Param("prixLimit") int prix);


}

package com.ecommerce.microcommerce.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


@NoArgsConstructor @AllArgsConstructor
@Getter @Setter 
@EqualsAndHashCode(callSuper = false, of = "code")
@ToString

@Entity
@Table(name = "discountCode")
public class DiscountCode extends Discount implements Serializable {
    
    private static final long serialVersionUID = 1L;
    private String code;

}

package com.ecommerce.microcommerce.model;

import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "lstProduct")
@ToString
public class Cart {

    private List<Product> lstProduct;
    private DiscountCode discountCode;
    private double price;
    private double totalPrice;

    /**
     * Calculate the cart price with all price products and discount.
     */
    public void priceCalculate() {
        this.price = 0;
        if (this.lstProduct != null) {
            for (Product p : this.lstProduct) {
                if (p.getDiscount() != null && p.getDiscount().isActive())
                    this.price += p.getPrice() - p.getDiscount().getPrice();
                else
                    this.price += p.getPrice();
            }
            if (discountCode == null || !discountCode.isActive())
                this.totalPrice = this.price;
            else
                this.totalPrice = this.price - discountCode.getPrice();
        }
        if (this.price < 0)
            this.price = 0;
        if (this.totalPrice < 0)
            this.totalPrice = 0;
    }

}

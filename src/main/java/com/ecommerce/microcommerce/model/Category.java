package com.ecommerce.microcommerce.model;


public enum Category {

    SHOOSES ("Shooses"),
    SHIRT ("Shirt"),
    DRESS ("Dress"),
    TROUSERS ("Trousers"),
    OTHER ("Other");

    private String name ="";

    //Constructor
    Category(String name){
        this.name = name;
    }

    public static Category fromName(String name) {
        for (Category c: Category.values()) {
            if(c.name.equalsIgnoreCase(name))
                return c;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                '}';
    }
}

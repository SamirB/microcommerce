package com.ecommerce.microcommerce.model;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.io.Serializable;

//@JsonIgnoreProperties(value = {"prixAchat", "id"})
//@JsonFilter("monFiltreDynamique")
@NoArgsConstructor @AllArgsConstructor
@Getter @Setter 
@EqualsAndHashCode(of = "id")
@ToString

@Entity
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private long id;
    @Length(min = 3, max = 20)
    private String name;
    private String reference;
    @Enumerated(EnumType.STRING)
    private Category category = Category.OTHER;
    private String description;

//    @Lob
//    private JPEG image;

    @Min(value = 1)
    private double price;
    @OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
    private Discount discount;

   

    public Product(@Length(min = 3, max = 20) String name, String reference,
                   Category category, String description, @Min(value = 1) double price, Discount discount) {
        this.name = name;
        this.reference = reference;
        this.category = category;
        this.description = description;
        this.price = price;
        this.discount = discount;
    }


}

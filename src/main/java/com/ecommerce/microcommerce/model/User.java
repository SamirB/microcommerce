package com.ecommerce.microcommerce.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.*;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor @AllArgsConstructor
@Getter @Setter 
@EqualsAndHashCode(of = "id")
@ToString
@Entity
public class User implements Serializable {

        private static final long serialVersionUID = 1L;

        @Id
        @GeneratedValue
        private long id;

        private String firstName;
        private String lastName;
        private String username;
        @Past
        private Date bornDate;
        private String phoneNumber;
        
        @Email
        private String mail;
        private String password;

        private double credits;
        @OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
        private Adress adress;

}

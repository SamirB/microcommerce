package com.ecommerce.microcommerce.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor @AllArgsConstructor
@Getter @Setter
@EqualsAndHashCode(of = "id")
@ToString

@Entity
@Table(name = "discount")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Discount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    private double price;

    private boolean isActive;

    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Temporal(TemporalType.DATE)
    private Date endDate;

    @PostLoad
    @PrePersist
    @PreUpdate
    public void calculateIsActive() {
        this.isActive = false;
        if (this.startDate != null && this.endDate != null) 
            if (this.startDate.before(new Date()) && this.endDate.after(new Date()))
                this.isActive = true;
    }

}

# microcommerce

Install projet :

- Install git https://gitforwindows.org/
- Open your terminal
- --> git clone <https://gitlab.com/user/projectname>

Launch project : 

- Install docker https://docs.docker.com/docker-for-windows/install/
- Open your terminal
- --> cd ./project/directory 
- --> docker-compose up

Urls exemples :

- http://localhost:8000/Produits --> get à JSON list
- http://localhost:8000/swagger-ui.html --> Swagger URL


More :

- You can find the image project to samirbouhassounwa/microserviceapp:v1.

- If you want to change ports, go to the docker-compose file.


